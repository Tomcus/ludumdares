# Ludum Dares

Collection of all Ludum Dare Entries

## Ludum Dare 45

Final themes:

* Start with nothing
* Unconventional travel
* Side effects
* Small character, big enemies
* Factory
* Shelter
* One item, many uses
* Night is coming
* Making connections
* Isolation
* Duplication
* Mutation
* Low Tech
* Take one, leave the rest
* Evolve to survive

Selected theme:

* Start with nothing

My project:

* TBA
