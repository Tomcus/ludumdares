# Themes and ideas

## Start with nothing

* simple RPG - rogue-like dungeon crawler
  * escape prison - can use some concepts from one item, many uses

## Unconventional travel

* Fores gnome travel system generator - create travel tracks for cretaures to transform gnomes from one tree-city to another - something like openttd but in fantasy world
* fox travel sevice simulator - more crazier the paths are the better prizes you get

## Side effects

* every action good action make bad reaction
* the better you do the worst the penalty is
* dungeon crawler rpg
* wizard game - less mana - crazier side effects
  * less mana - you become more vunerable to magic - starts messing with you

## Small character, big enemies

* hardcore slash and hack action game - Dark Souls
* or same enemy but every time you defat him you become smaller and weaker

## Factory

* simple factory building game
* shooter (1930s gangs - liquer factory)
  * play as police officer busting ilegal alcohol storage and production facilities

## Shelter

* running from storm - quick looting
* also can use Night is coming idea

## One item, many uses

* Wizard FPS shooter - one wand - limited number of uses - recharging
* OR any wizard game (like Escape From Azkaban) - same concept ^^^ (but can be 2D)
* or the 1930s gang game in Factory

## Night is coming

* Quick towerdefence game
  * Alone on island - defend only access
  * Assign people to defence (place units) or buildings (produce resources)

## Making connections

* Same as Unconventional travel

## Isolation

* Two dimensions idea but changing view is not dependent on player but based on move time (more time less view changes)

## Duplication

* 2 dimesnions, duplicated characters, diferent puzzles, controls bowth at the time

## Mutation

* same as Evolve to survive

## Low Tech

* Retro game
* Knight vs. Machines

## Take one, leave the rest

* Rogue-like rpg - only one item to hold
* logic game - can carry just one item (lot of gates and gate triggers - bring item to altar - final location) - one item on one tile

## Evolve to survive

* pokemon-like rpg - playing as pokemon
  * createrue evolves into more powerfull creatures
